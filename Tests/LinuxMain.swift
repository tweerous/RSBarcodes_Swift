import XCTest

import RSBarcodesTests

var tests = [XCTestCaseEntry]()
tests += RSBarcodesTests.allTests()
XCTMain(tests)
